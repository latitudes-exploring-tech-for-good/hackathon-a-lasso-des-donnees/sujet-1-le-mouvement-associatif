# # Sujet n°1 : Vie associative des Pays de la Loire - Le Mouvement Associatif (https://lemouvementassociatif.org/)

+ Thème : Valorisation et transparence de la vie associative.
+ Phase d'avancement actuelle : au stade d'idée.
+ Compétences associés : data science, data visualisation, design.


##### #1 | Présentation de Le Mouvement associatif 
Avec 20 millions de bénévoles et 1,8 million de salariés, le monde associatif est une des forces vives les plus puissantes de notre pays. Sur tous les fronts, sur l’ensemble des territoires et à l’international, au plus près des besoins, les associations agissent et innovent chaque jour au service de l’intérêt général. Rassemblant près de 600 000 associations, Le Mouvement associatif est le porte-voix de ces dynamiques pour favoriser le développement d’une force associative utile et créative. 


##### #2 | Problématique
Comment donner à voir aux citoyens l’importance de la vie associative dans leur territoire ? 
Comment favoriser la transparence des décisions publiques de soutien aux associations ? 
Comment valoriser la contribution des associations aux territoires dans lesquels elles déploient leurs actions qu’elles portent ? 
Voici quelques questions auxquelles nous pourrons essayer de répondre en combinant les nombreuses données disponibles sur le sujet. 


##### #3 | Le défi proposé
La vie associative en région Pays de la Loire ! 

Cette région dispose des conditions favorables pour imaginer des combinaisons de données inédites sur un même territoire. 
En effet, en plus des données dont disposent l’Etat, des collectivités territoriales sont volontaires et prêtes à ouvrir leurs données disponibles sur la vie associative. 
Afin de rendre accessible ces informations au plus grand nombre, cette réflexion doit également intégrer les enjeux de bonne mise en visualisation des données.


##### #4 | Livrables
Objectifs : cartographie, moteur de recherche, ou toute proposition que vous jugerez pertinente.


##### #5 | Ressources à disposition pour résoudre le défi

+ La liste établie par ETALAB des jeux de données relatifs aux associations : https://www.data.gouv.fr/fr/posts/les-jeux-de-donnees-des-associations/
+ Les jeux de données mis à disposition par des collectivités en Pays de la Loire.
+ Des mentors pour vous accompagner dans la découverte et l'utilisation des ressources. 


##### #6 | Code de conduite et philosophie du hackathon
Lors de la conception du hackathon, Latitudes a voulu prendre des partis-pris afin de rendre celui-ci particulier. 

Il s’agit d’éléments que nous souhaitons incarner collectivement avec vous tout au long de la journée :
+ La force du collectif pour faire émerger des solutions adaptées aux porteur.se.s de projets, notamment via la session de stand-up meeting ;
+ Une attention portée à la conception rapide, au test et à l’itération qui doivent permettre aux porteur.se.s de projets d’avoir accès aux solutions les plus abouties possibles ;
+ L’engagement sur des solutions ouvertes, facilement utilisables par les porteur.se.s de projets, et qui vous permettront de continuer à contribuer à l’issue du hackathon si vous le souhaitez, ou de permettre à d’autres personnes de le faire.

##### #7 | Points de contact lors du hackathon
+ Ahmed EL KHADIRI : Responsable du Développement et Animation de réseau du Mouvement associatif. 